//
//  ViewController.swift
//  initMultimedia
//
//  Created by AlexM on 19/03/15.
//  Copyright (c) 2015 AMO. All rights reserved.
//

import UIKit
import MediaPlayer

class ViewController: UIViewController {
    var theMovie: MPMoviePlayerController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        playVideo()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"orientationChanged", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func playVideo() {
        var bundle = NSBundle.mainBundle()
        var moviePath = bundle.pathForResource("001", ofType: "mp4")
        var url = NSURL(fileURLWithPath: moviePath!)
        
        theMovie = MPMoviePlayerController(contentURL: url)
        theMovie.view.frame = CGRectMake(0, 0, 270, 270)
        self.view.addSubview(theMovie.view)
        theMovie.play()
    }
    
    func orientationChanged()
    {
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            var width = UIScreen.mainScreen().bounds.width
            var height = UIScreen.mainScreen().bounds.height
            
            theMovie.view.frame = CGRectMake(0, 0, width, height)
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            var width = UIScreen.mainScreen().bounds.width
            var height = UIScreen.mainScreen().bounds.height
            
            theMovie.view.frame = CGRectMake(0, 0, width, height)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

