//
//  MainViewController.swift
//  initMultimedia
//
//  Created by AlexM on 19/03/15.
//  Copyright (c) 2015 AMO. All rights reserved.
//

import UIKit
import MobileCoreServices

class MainViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func usePhotoLibrary(sender: AnyObject) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.PhotoLibrary)!
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }

    @IBAction func useCamera(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.Camera)!
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        
        else {
            let alertController = UIAlertController(title: "Error with Camera", message:
                "Camera isn't available in this device", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    //Methods to control the use of Camera and PhotLibrary
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                
            if (info[UIImagePickerControllerMediaType] as! NSString == (NSString)(format: kUTTypeMovie)){
                let movieURL = info[UIImagePickerControllerMediaURL] as! NSURL
                
                //Save in PhotoLibrary
                let movieData = NSData(contentsOfURL: movieURL)
                if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(movieURL.path)){
                        UISaveVideoAtPathToSavedPhotosAlbum(movieURL.path, self,"video:didFinishSavingWithError:contextInfo:", nil)
                }
            }
                
            else {
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                let imageData = UIImagePNGRepresentation(image) as NSData
                UIImageWriteToSavedPhotosAlbum(image, self, "image:didFinishSavingWithError:contextInfo:", nil)
                imageView.image = UIImage(data: imageData)
            }
                
        }
        
        else if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            let imageData = UIImagePNGRepresentation(image) as NSData
                
            imageView.image = UIImage(data: imageData)
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //Implemented methods to control erros when the media is saved
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo: UnsafePointer<()>){
        if (error != nil) {
            println("ERROR IMAGE \(error.debugDescription)")
        }
    }
    
    func video(video: NSString, didFinishSavingWithError error: NSErrorPointer, contextInfo: UnsafePointer<()>){
        if (error != nil){
            println("ERROR VIDEO \(error.debugDescription)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
